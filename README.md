# README #

This project is a copy from https://java.net/projects/brazilutils/ .

The license is under Apache License v2.0 .

The original project is under SVN. The working tag is marked with the name BrazilUtils_0_1 .

### What is this repository for? ###

I learned about this project working for a contractor. They used this project to normalize some informations about brazilians documents. At that moment I could not find any Maven Repos, to insert this project to a Nexus Server I have cloned this code at Bitbucket and added some instructions to help others.

### How do I get set up? ###

First of all you will need to work with the tag: git checkout origin/tags/BrazilUtils_0_1 

I am a linux user, so to compile using this package using ANT, I had some problems with char encodings with ISO-8859-1.

Before compile you need to set global var ANT_OPTS to -Dfile.encoding=iso-8859-1. 

Issuing the command **ant build** to Build the project.

To install the resulting library to a local maven repository in .m2/repository, try this command:

> mvn install:install-file -Dfile=./BrazilUtils/bin/brazilutils.0.0.1.jar -DgroupId=com.brazilutils -DartifactId=brazilutils -Dversion=0.0.1 -Dpackaging=jar
  
### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
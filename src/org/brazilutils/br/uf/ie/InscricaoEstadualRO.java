/*
 * Created on 07/05/2005
 */
package org.brazilutils.br.uf.ie;

/**
 * Represents the Inscricao Estadual of Rondonia - RO<br>
 * 
 * <a href="http://www.sintegra.gov.br/Cad_Estados/cad_RO.html">
 * http://www.sintegra.gov.br/Cad_Estados/cad_RO.html</a>
 * 
 * @author Douglas Siviotti
 */
public final class InscricaoEstadualRO extends InscricaoEstadual {
    
    public static final int    DIGIT_COUNT = 14;
    public static final int    OLD_DIGIT_COUNT = 9;
    public static final String MASK = "########.#####-#";
    public static final String OLD_MASK = "###.#####-#";

    /**
     * Converts the number from 101.62521-3 to 0000000062521-3.  
     * Old Format -> New Fotmat
     */
    public void convertToNewFormat(){

        // The oldNUmber must be in the Old Format
        if (getNumber().length() == OLD_DIGIT_COUNT) {
        	
            String result = getNumber().substring(3);
        
            for (int i=result.length() ; i < DIGIT_COUNT; i++){
                result = "0" + result ;
            }
            setNumber(result); 
        }
    }
    /** 
     * @see org.brazilutils.br.uf.ie.InscricaoEstadual#defaultDigitCount()
     */
    public int defaultDigitCount() {
        if(getNumber() != null && getNumber().length() == OLD_DIGIT_COUNT){
            return OLD_DIGIT_COUNT;
        }
        return DIGIT_COUNT;
    }

    /** 
     * @see org.brazilutils.br.uf.ie.InscricaoEstadual#getDvCount()
     */
    public int getDvCount() {
        return 1;
    }

    /** 
     * @see org.brazilutils.utilities.NumberComposed#getMask()
     */
    public String getMask() {
        if(getNumber() != null && getNumber().length() == OLD_DIGIT_COUNT){
            return OLD_MASK;
        }
        return MASK;
    }

    /** 
     * @see org.brazilutils.br.uf.ie.InscricaoEstadual#defineCoeficients()
     */
    public void defineCoeficients() {
        setCoeficientList("6543298765432");
    }
    
    /**
     * 
     */
    public boolean isValid() {
        convertToNewFormat();
        return super.isValid();
    }
}

/*
 * Created on 07/05/2005
 */
package org.brazilutils.br.uf.ie;

/**
 * Represents the Inscricao Estadual of Rio Grande do Norte - RN<br>
 * 
 * <a href="http://www.sintegra.gov.br/Cad_Estados/cad_RN.html">
 * http://www.sintegra.gov.br/Cad_Estados/cad_RN.html</a>
 * 
 * @author Douglas Siviotti
 */
public final class InscricaoEstadualRN extends InscricaoEstadual {

    public static final int    OLD_DIGIT_COUNT = 9;
    public static final int    DIGIT_COUNT = 10;
    public static final String OLD_MASK = "##.###.###-#";
    public static final String MASK = "##.#.###.###-#";
    
    /** 
     * @see org.brazilutils.br.uf.ie.InscricaoEstadual#defaultDigitCount()
     */
    public int defaultDigitCount() {
        if(getNumber() != null && getNumber().length() == OLD_DIGIT_COUNT){
            return OLD_DIGIT_COUNT;
        }
        return 10;
    }

    /** 
     * @see org.brazilutils.br.uf.ie.InscricaoEstadual#getDvCount()
     */
    public int getDvCount() {
        return 1;
    }

    /** 
     * @see org.brazilutils.utilities.NumberComposed#getMask()
     */
    public String getMask() {
        if(getNumber() != null && getNumber().length() == OLD_DIGIT_COUNT){
            return OLD_MASK;
        }
        return MASK;
    }

    /** 
     * @see org.brazilutils.br.uf.ie.InscricaoEstadual#defineCoeficients()
     */
    public void defineCoeficients() {
        setCoeficientList("98765432");
    }

    /** 
     * <a href="http://www.sintegra.gov.br/Cad_Estados/cad_RN.html">
     * http://www.sintegra.gov.br/Cad_Estados/cad_RN.html</a>
     * @see org.brazilutils.validation.Validable#isValid()
     */
    public boolean isValid() {
        int sum; // Sum of Multiply (Digit * Peso)
        int mod; // Module in sum % 11 or sum % 10
        int dv1; // Fisrt Calculated Chek Digit

        //State Digits : 2(pos 0) and 0(pos 1)
        if ( (!isFixDigitCorrect(0, '2')) && (!isFixDigitCorrect(1, '0')) ) {
            return false; 
        }
        
        // Necessario para que a validacao em cadeia (chain validation) funcione.
    	defineCoeficients();
        
        // Adicionado o Coeficiente maior que o numero nove(9) para realizar o calculo
    	if(getNumber().length()== DIGIT_COUNT){
    	    addCoeficientLeft(10);
        }
        
        // If the Digit Count is not correct return false
        if (!isValidDigitCount()) {
        	return false;
        }
        
        // Calculate the Check Digit
        sum = getCalcSum() * 10;
        mod = sum - (sum / 11) * 11;
        if (mod == 10) {
            dv1 = 0; 
        } else { 
            dv1 = mod;
        }
        
        // Returns Calculated Chek Digit = The Real Check Digit
        return dv1 == getDv1();        
    }
    
}

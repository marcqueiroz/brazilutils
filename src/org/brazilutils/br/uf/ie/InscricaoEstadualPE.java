/*
 * Created on 07/05/2005
 */
package org.brazilutils.br.uf.ie;

/**
 * Represents the Inscricao Estadual of Pernambuco - PE<br>
 * 
 * <a href="http://www.sintegra.gov.br/Cad_Estados/cad_PE.html">
 * http://www.sintegra.gov.br/Cad_Estados/cad_PE.html</a>
 * 
 * @author Douglas Siviotti
 */
public final class InscricaoEstadualPE extends InscricaoEstadual {

    public static final int    DIGIT_COUNT = 9;
    public static final int    OLD_DIGIT_COUNT = 14;
    public static final String MASK = "###.####-##";
    public static final String OLD_MASK = "##.#.###.#######-#";
    
    
    /** 
     * @see org.brazilutils.br.uf.ie.InscricaoEstadual#defaultDigitCount()
     */
    public int defaultDigitCount() {
        if(getNumber() != null && getNumber().length() == OLD_DIGIT_COUNT){
            return OLD_DIGIT_COUNT;
        }
        return DIGIT_COUNT;
    }

    /** 
     * @see org.brazilutils.br.uf.ie.InscricaoEstadual#getDvCount()
     */
    public int getDvCount() {
        if(getNumber() != null && getNumber().length() == OLD_DIGIT_COUNT){
            return 1;
        }
        return 2;
    }

    /** 
     * @see org.brazilutils.utilities.NumberComposed#getMask()
     */
    public String getMask() {
        if(getNumber() != null && getNumber().length() == OLD_DIGIT_COUNT){
            return OLD_MASK;
        }
        return MASK;
    }

    /** 
     * @see org.brazilutils.br.uf.ie.InscricaoEstadual#defineCoeficients()
     */
    public void defineCoeficients() {
        // Define o coeficiente para a maneira antiga de validacao
        setCoeficientList("5432198765432");
    }

    /**
     * <a href="http://www.sintegra.gov.br/Cad_Estados/cad_PE.html">
     *http://www.sintegra.gov.br/Cad_Estados/cad_PE.html</a>
     * @see org.brazilutils.validation.Validable#isValid()
     */
    public boolean isValid() {
        return (isValidNew() || isValidOld());
    }

    /**
     * 
     * @return boolean
     */
    private boolean isValidOld(){
        int sum; // Sum of Multiply (Digit * Peso)
        int mod; // Module in sum % 11 or sum % 10
        int dv1; // Fisrt Calculated Chek Digit
        
        // Necessário para que a validação em cadeia (chain validation) funcione.
        defineCoeficients();
        
        // If the Digit Count is not correct return false
        if (!isValidDigitCount()) {
            return false;
        }
        
        // Calculate the Check Digit
        sum = getCalcSum();
        mod = sum % 11;
              
        if ( 11 - mod > 9) { // New Code
            dv1 = 11 - mod - 10;  
        } else { 
            dv1 = 11 - mod;
        }

        //Returns Calculated Chek Digit = The Real Check Digit
        return dv1 == getDv1(); 
    }
    
    /**
     * 
     * @return boolean
     */
    private boolean isValidNew(){
        int i; int j;   // just count 
        int digit;      // A number digit
        int coeficient; // A coeficient  
        int sum;        // The sum of (Digit * Coeficient)
        int[] foundDv = {0,0}; // The found Dv1 and Dv2
        String ie = getNumber(); 
        int dv1 = Integer.parseInt(String.valueOf(ie.charAt(ie.length()-2)));
        int dv2 = Integer.parseInt(String.valueOf(ie.charAt(ie.length()-1)));       

        //If the Digit Count is not correct return false
        if (!isValidDigitCount()) {
            return false;
        }        
        
        for (j = 0; j < 2; j++) {
            sum = 0;
            coeficient = 2;
            for (i = ie.length() - 3 + j; i >= 0 ; i--){
                digit = Integer.parseInt(String.valueOf(ie.charAt(i)));               
                sum += digit * coeficient;
                coeficient ++;
                if (coeficient > 9) coeficient = 2;                
            }                
            foundDv[j] = 11 - sum % 11;
            if (foundDv[j] >= 10) foundDv[j] = 0;
        }
        return dv1 == foundDv[0] && dv2 == foundDv[1];
    }
}

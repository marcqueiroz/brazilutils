/**
 * This interface contains velocity units 
 * <ul>
 * <li> The unit base is kilometer-hour  
 * </ul>
 *  
 * @author D'Artagnan Ramos Dias Neto
 * @version 0.1
 * 
 */

package org.brazilutils.metrics;

import java.math.BigDecimal;

public interface VelocityUnits {
	
	Unit KILOMETER_PER_HOUR = new Unit("km/h", new BigDecimal(String.valueOf(1)));
	Unit METER_PER_SECOND = new Unit("m/s", new BigDecimal(String.valueOf(0.27777)));
	Unit MILES_PER_HOUR = new Unit("mi/h", new BigDecimal(String.valueOf(1.609)));
	Unit NAUTIC_MILES_PER_HOUR = new Unit("nmi/h", new BigDecimal(String.valueOf(1.852)));
	Unit BASE=KILOMETER_PER_HOUR;
}

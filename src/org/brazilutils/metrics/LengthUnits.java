package org.brazilutils.metrics;
import java.math.*;
/**
 * This interface contains units to perform Length conversion
 * <ul>
 * <li> The component base is Meter Unit 
 * </ul>
 *  
 * @author D'Artagnan Ramos Dias Neto
 * @author Eduardo Machado de Oliveira
 * @version 0.1
 * 
 */

public interface LengthUnits {
	/**
	 * meter is the standard basis
	 */ 
	Unit METER=new Unit("m",new BigDecimal(String.valueOf(1)));
	Unit BRACES=new Unit("br",new BigDecimal(String.valueOf(0.546806649)));
	Unit CADEIAS=new Unit("ca",new BigDecimal(String.valueOf(0.0497096954)));
	Unit YARD=new Unit("y",new BigDecimal(String.valueOf(1.093613298)));

	Unit MILES=new Unit("mi",new BigDecimal(String.valueOf(0.000621371)));
	Unit NAUTIC_MILES=new Unit("nmi",new BigDecimal(String.valueOf(0.000539956803)));
	Unit FEET=new Unit("f",new BigDecimal(String.valueOf(3.280839895)));
	Unit INCH=new Unit("in",new BigDecimal(String.valueOf(39.37007874)));
	Unit KILOMETER=new Unit("km",new BigDecimal(String.valueOf(0.001)));

}

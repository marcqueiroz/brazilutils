/*
 * Created on 23/04/2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.brazilutils.metrics;

/**
 * @author D�Artagnan Ramos Dias Neto
 */
public class Length {

	/**
	 * Constructor method
	 *  
	 */
	public Length() {

	}

	/**
	 * @param distance
	 * @return double
	 */
	public double milesToKilometer(final double distance) {
		return distance * 1.609;

	}
   
	/**
	 * @param distance
	 * @return double
	 */ 
	public double kilometerToMiles(final double distance){
		return distance / 1.609;
	}
	
	/**
	 * @param extension
	 * @return double
	 */	
	public double centimeterToInch(final double extension){
		return extension / 2.54;
	}
	
	/**
	 * @param extension
	 * @return double
	 */
	public double inchToCentimeter(final double extension){
		return extension * 2.54;
	}
	
	/**
	 * @param extension
	 * @return
	 */
	public double inchToMeter(final double extension){
		return extension / 39.37;
	}
	
	/**
	 * @param extension
	 * @return
	 */
	public double meterToInch(final double extension){
		return extension * 39.37;
	}
	
	/**
	 * @param extension
	 * @return double
	 */
	public double feetToInch(final double extension){
		return extension * 12;
	}
	
	/**
	 * @param extension
	 * @return double
	 */
	public double inchToFeet(final double extension){
		return extension / 12;
	}
	
	/**
	 * @param extension
	 * @return double
	 */
	public double feetToMeter(final double extension){
		return extension * 3.28;		
	}
	
	/**
	 * @param extension
	 * @return double
	 */
	public double meterToFeet(final double extension){
		return extension/3.28;
	}
	
 }
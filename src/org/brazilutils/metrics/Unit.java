/**
 * Unit is the class base to permits metrics conversion into other. A Unit
 * object encapsulates the base metric needed for a given Metric System.
 * 
 * @author D�Artagnan Ramos Dias Neto
 * @author Eduardo Machado de Oliveira
 * @version 0.1
 * 
 */

package org.brazilutils.metrics;

import java.math.*;

public class Unit {

	private String name;
	private BigDecimal valueOnBaseUnit;
	
	/**
	 * Creates new Unit metric
	 * 
	 * WARNING: Do NOT modify this code. The content of this constructor is
	 * always called for all the interface with metric units.
	 * 
	 * @param name  name of metric unit
	 * @param value the value of the metric unit
	 */
	public Unit(String name, BigDecimal valueOnBaseUnit) {
		this.name = name;
		this.valueOnBaseUnit = valueOnBaseUnit;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}
	
	/**
	 * Getter for property value  
	 * @return Value of property value.
	 */
	public BigDecimal getValueOnBaseUnit() {
		return valueOnBaseUnit;
	}
	
	/**
	 * Setter for property value.
	 * @param unitValue New value of property unitValue.
	 */
	public void setValueOnBaseUnit(final BigDecimal valueOnBaseUnit) {
		this.valueOnBaseUnit = valueOnBaseUnit;
	}

	public String toString() {
		return getValueOnBaseUnit().toString();
	}

}

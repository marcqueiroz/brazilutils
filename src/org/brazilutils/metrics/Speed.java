/*
 * Created on 23/04/2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.brazilutils.metrics;

/**
 * @author D�Artagnan Ramos Dias Neto
 */
public class Speed {
	
	/**
	 * @param speed
	 * @return double
	 */
	public double machToKilometersPerHour(final double speed){
		return speed * 1226;
	}
	
	/**
	 * @param speed
	 * @return double
	 */
	public double kilometerToMachPerHour(final double speed){
		return speed / 1226;
	}	
	
	/**
	 * @param miles
	 * @return double
	 */
	public double milesToKilometerPerHour(final int miles){
		return miles * 1.609;
	}
	
	/**
	 * @param kilometers
	 * @return double
	 */
	public double kilometerToMilesPerHour(final int kilometers){
		return kilometers / 1.609;
	}
}
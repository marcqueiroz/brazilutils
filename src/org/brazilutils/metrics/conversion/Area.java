package org.brazilutils.metrics.conversion;

import java.math.BigDecimal;
import org.brazilutils.metrics.*;

/**
 * Area is the class which allow an application to convert area metrics into
 * other area metrics
 * 
 * @author D�Artagnan Ramos Dias Neto
 * @author Eduardo Machado de Oliveira
 * @version 0.1
 * 
 */

public class Area implements UnitWithValue, AreaUnits {

	private static final long serialVersionUID = 210046788217078583L;

	private Unit unit;

	private transient BigDecimal value;

	private final transient BigDecimal base;

	public Area(BigDecimal value, Unit unit) {
		this.base = value.multiply(unit.getValueOnBaseUnit());
		this.value = value;
		this.unit = unit;
	}

	public UnitWithValue convertTo(
			final Unit unit, final int scale, final int roundingmode) {
		
		return new Area(
				base.divide(unit.getValueOnBaseUnit(), scale, roundingmode), 
				unit);
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(final Unit u) {
		this.unit = u;
		this.value = base.divide(unit.getValueOnBaseUnit());
	}

	public BigDecimal getValue() {
		return value;
	}

	public String toString() {
		return getValue().toString();
	}
}

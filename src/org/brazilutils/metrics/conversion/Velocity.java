package org.brazilutils.metrics.conversion;

import java.math.BigDecimal;

import org.brazilutils.metrics.Unit;
import org.brazilutils.metrics.UnitWithValue;
import org.brazilutils.metrics.VelocityUnits;

/**
 * Velocity is the class which allow an application
 * to convert velocity units into other velocity units.
 *  
 * @author D�Artagnan Ramos Dias Neto
 * @author Eduardo Machado de Oliveira
 * @version 0.1
 * 
 */

public class Velocity implements UnitWithValue, VelocityUnits {
	private static final long serialVersionUID = 211046785217178583L;
	private Unit unit;
	private transient BigDecimal value;
	private final transient BigDecimal base;
	
	public Velocity(BigDecimal value,Unit unit){
		this.base = value.multiply(unit.getValueOnBaseUnit());
		this.value = value;
		this.unit = unit;
	}
		
	public UnitWithValue convertTo(
			final Unit unit, final int scale, final int roundingmode) {
		
		return new Area(
				base.divide(unit.getValueOnBaseUnit(),scale,roundingmode),
				unit);
	}
		
	public Unit getUnit(){
		return unit;
	}
	
	public void setUnit(final Unit u) {
		this.unit = u;
		this.value = base.divide(unit.getValueOnBaseUnit());
	}
	
	public BigDecimal getValue(){
		return value;
	}
	
	public String toString() {
		return getValue().toString();
	}
}

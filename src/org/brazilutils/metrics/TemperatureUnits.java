package org.brazilutils.metrics;

import java.math.*;

/**
 * This interface contains temperature conversion units
 * <ul>
 * <li> The base unit is the degree Celsius
 * </ul>
 * 
 * @author D'Artagnan Ramos Dias Neto
 * @version 0.1 
 */

public interface TemperatureUnits {
	Unit CELSIUS = new Unit("C", new BigDecimal(String.valueOf(0)));
	Unit FAHRENHEIT=new Unit( "F",new BigDecimal(String.valueOf(32) ));
	Unit RANKINE=new Unit("Ra",new BigDecimal(String.valueOf(491.67)));
	Unit REAUMUR=new Unit("R�",new BigDecimal(String.valueOf(0)));
	Unit KELVIN=new Unit("K",new BigDecimal(String.valueOf(273.15)));
	Unit BASE=CELSIUS;
}

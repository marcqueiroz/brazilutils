/*
 * Created on 23/05/2005
 */
package org.brazilutils.metrics;
/**
 * @author D�Artagnan Ramos Dias Neto
 *
 */

public class Power {
   
   /**
    * @param energy
    * @return double
    */
   public double hpToWatt(final double energy){      
	   return energy * 745.7;
   }
   
   /**
    * @param energy
    * @return double
    */   
   public double wattToHp(final double energy){
	   return energy / 745.7;
   }	
}
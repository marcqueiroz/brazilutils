/**
 * This interface contains Area units
 * <ul>
 * <li> The component base is Square Meter Unit 
 * </ul>
 *  
 * @author D'Artagnan Ramos Dias Neto
 * @author Eduardo Machado de Oliveira
 * @version 0.1
 * 
 */

package org.brazilutils.metrics;

import java.math.*;

public interface AreaUnits {
	 /**
	  * Square Metric is the unit base for area conversion
	  */
	Unit M2 = new Unit("m�", new BigDecimal(String.valueOf(1)));
	Unit ACRE = new Unit("ac", new BigDecimal(String.valueOf(0.000247105)));
	Unit ARE = new Unit("a", new BigDecimal(String.valueOf(0.01)));
	Unit ALQUEIRES_MINEIRO = new Unit("Alqueire Mineiro", new BigDecimal(
			String.valueOf(0.00020661157)));
	Unit ALQUEIRES_DO_NORTE = new Unit("Alqueire_do_Norte", new BigDecimal(
			String.valueOf(0.000367309458)));
	Unit ALQUEIRES_PAULISTA = new Unit("Alqueire Paulista", new BigDecimal(
			String.valueOf(0.00041322314)));
	Unit SQUARE_BRACAS = new Unit("br�", new BigDecimal(String
			.valueOf(0.000327653997)));/* Brazilian Metric-SQUARE_BRACES */
	Unit BRACAS_DE_SESMARIA = new Unit("br_de_sesmaria", new BigDecimal(
			String.valueOf(0.000688705234)));	
	Unit HECTARE = new Unit("ha", new BigDecimal(String.valueOf(0.0001)));
	Unit SQUARE_YARD = new Unit("yd�", new BigDecimal(String
			.valueOf(1.195990046)));
	Unit SQUARE_MILE = new Unit("mi�", new BigDecimal(String
			.valueOf(0.000003861021585)));
	Unit SQUARE_FOOT = new Unit("ft2", new BigDecimal(String
			.valueOf(10.763910417)));
	Unit SQUARE_QUADRAS = new Unit("qq�", new BigDecimal(
			String.valueOf(0.000573921028)));
	Unit SQUARE_KILOMETER = new Unit("km�", new BigDecimal(
			String.valueOf(0.000001)));
	Unit BASE = M2;
}

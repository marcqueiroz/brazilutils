/*
 * Created on 23/04/2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.brazilutils.metrics;

/**
 * @author D�Artagnan Ramos Dias Neto
 */
public class Volume {
	
	/**
	 * @param volume
	 * @return double 
	 */
	public double cubicMeterToLitre(final int volume){
		return volume * 1000;
	}
	
	/**
	 * @param litres
	 * @return double
	 */
	public double litreToCubicMeter(final double litres){
		return litres / 1000;
	}
	
	/**
	 * @param litres
	 * @return double
	 */
	public double litreToUSgallon(final double litres){
		return litres / 3.785;
	}
	
	/**
	 * @param gallon
	 * @return double
	 */
	public double usGallonToLitre(final double gallon){
		return gallon * 3.785;
	}
}
/*
 * Created on 23/04/2005
 */
package org.brazilutils.metrics;

/**
 * @author D�Artagnan Ramos Dias Neto
 */

public class Temperature {

    /**
     * Comment for <code>ABSOLUTE_ZERO</code>Zero absoluto ou Zero Kevin.
     * Temperatura em �C. Conforme ITS-90.
     */
    public static final double ABSOLUTE_ZERO = -273.15;

    /**
     * Comment for <code>OXYGEN_BOILING</code>Ebuli��o do Oxig�nio.
     * Temperatura em �C. Conforme ITS-90.
     */
    public static final double OXYGEN_BOILING = -182.954;

    /**
     * Comment for <code>WATER_TRIPLE_POINT</code>Ponto Triplo da �gua.
     * Temperatura em �C. Conforme ITS-90.
     */
    public static final double WATER_TRIPLE_POINT = 0.010;

    /**
     * Comment for <code>STANNUM_SOLIDIFICATION</code>Solidifica��o do
     * Estanho. Temperatura em �C. Conforme ITS-90.
     */
    public static final double STANNUM_SOLIDIFICATION = +231.928;

    /**
     * Comment for <code>ZINC_SOLIDIFICATION</code>Solidifica��o do Zinco.
     * Temperatura em �C. Conforme ITS-90.
     */
    public static final double ZINC_SOLIDIFICATION = +419.527;

    /**
     * Comment for <code>ARGENTUM_SOLIDIFICATION</code>Solidifica��o do
     * Prata. Temperatura em �C. Conforme ITS-90.
     */
    public static final double ARGENTUM_SOLIDIFICATION = +961.780;

    /**
     * Comment for <code>AURUM_SOLIDIFICATION</code>Solidifica��o do Ouro.
     * Temperatura em �C. Conforme ITS-90.
     */
    public static final double AURUM_SOLIDIFICATION = +1064.180;

    public Temperature() {
    }

    /**
     * @param celsius
     */
    public double CelsiusToKelvin(final double celsius) {
    	return celsius + 273.15;
    }

    /**
     * @param kelvin
     */
    public double KelvinToCelsius(final double kelvin) {
    	return kelvin - 273.15;
    }

    /**
     * @param celsius
     */
    public double CelsiusToFarenheit(final double celsius) {
    	return (celsius * 9 / 5) + 32;
    }

    /**
     * @param kelvin
     */
    public double KelvinToFarenheit(final double kelvin) {
    	return this.CelsiusToFarenheit(this.KelvinToCelsius(kelvin));
    }

    /**
     * @param farenheit
     */
    public double FarenheitToCelsius(final double farenheit) {
    	return (farenheit - 32) * 5 / 9;
    }

    /**
     * @param farenheit
     */
    public double FarenheitToKelvin(final double farenheit) {
    	return this.CelsiusToKelvin(this.FarenheitToCelsius(farenheit));
    }

    /**
     * @param rankine
     */
    public double RankineToFarenheit(final double rankine) {
    	return rankine - 459.67;
    }

    /**
     * @param farenheit
     */
    public double FarenheitToRankine(final double farenheit) {
    	return farenheit + 459.67;
    }

    /**
     * @param reamur
     */
    public double ReamurToCelsius(final double reamur) {
    	return reamur * 5 / 4;
    }

    /**
     * @param celsius
     */
    public double CelsiusToReamur(final double celsius) {
    	return celsius * 4 / 5;
    }
}
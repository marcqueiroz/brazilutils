package org.brazilutils.metrics;
/**
 * This interface contains conversion
 * method convertTo
 *   
 * @author Eduardo Machado de Oliveira
 * @version 0.1
 * 
 */


public interface UnitWithValue {
	
	UnitWithValue convertTo(Unit unit, int scale, int roudingmode);

}

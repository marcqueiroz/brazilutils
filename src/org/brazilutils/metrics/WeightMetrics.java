package org.brazilutils.metrics;

import java.math.BigDecimal;

/**
 * This interface contains permits Weight conversion
 * <ul>
 * <li> The component base is Gram Unit 
 * </ul>
 *  
 * @author D'Artagnan Ramos Dias Neto
 * @version 0.1
 * 
 */

public interface WeightMetrics {
	Unit GRAM =new Unit("g",new BigDecimal(String.valueOf(1)));
	Unit DECIGRAM =new Unit("dg",new BigDecimal(String.valueOf(10)));
	Unit CENTIGRAM =new Unit("cg",new BigDecimal(String.valueOf(100)));
	Unit DEKAGRAM =new Unit("dg",new BigDecimal(String.valueOf(0.1)));
	Unit HECTOGRAM =new Unit("hg",new BigDecimal(String.valueOf(0.01)));
	Unit KILOGRAM =new Unit("kg",new BigDecimal(String.valueOf(0.001)));
	Unit MEGAGRAM =new Unit("Mg",new BigDecimal(String.valueOf(0.000001)));
	Unit MICROGRAM =new Unit("ug",new BigDecimal(String.valueOf(1000000)));
	Unit MILIGRAM =new Unit("mg",new BigDecimal(String.valueOf(1000)));
	Unit NANOGRAM =new Unit("ng",new BigDecimal(String.valueOf(0.000000001)));
	Unit PICOGRAM =new Unit("pg",new BigDecimal(String.valueOf(0.000000000001)));
	Unit TONNE =new Unit("t",new BigDecimal(String.valueOf(0.000001)));
	Unit METRIC_TON= new Unit("mt",new BigDecimal(String.valueOf(0.000001)));
	Unit SHORT_TON= new Unit("st",new BigDecimal(String.valueOf(0.000001102)));
	Unit OUNCE= new Unit("oz",new BigDecimal(String.valueOf(0.035273962)));	
	Unit BASE = GRAM;
}

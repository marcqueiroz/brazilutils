/*
 * Created on 23/04/2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.brazilutils.metrics;

/**
 * @author D�Artagnan Ramos Dias Neto
 */
public class Mass { 
	
	/**
	 * @param heigth
	 * @return double
	 */
	public double kilogramToPound(final double heigth){
		return heigth * 2.2046;
	}
	
	/**
	 * @param heigth
	 * @return double
	 */
	public double poundToKilogram(final double heigth){
		return heigth / 2.2046;
	}
	
	/**
	 * @param heigth
	 * @return double
	 */
	public double gramToPound(final double heigth){
		return heigth / 453.6;
	}
	
	/**
	 * @param heigth
	 * @return double
	 */
	public double poundToGram(final double heigth){
		return heigth * 453.6;
	}
	
	/**
	 * @param heigth
	 * @return double
	 */
	public double kilogramToLug(final double heigth){
		return heigth * 0.06852;
	}
	
	/**
	 * @param heigth
	 * @return double
	 */
	public double lugToKilogram(final double heigth){
		return heigth / 0.06852;
	}
	
	/**
	 * @param heigth
	 * @return double
	 */
	public double poundToLug(final double heigth){
		return heigth * 0.03108;
	}
	
	/**
	 * @param heigth
	 * @return double
	 */
	public double lugToPound(final double heigth){
		return heigth / 0.03108;
	}
}

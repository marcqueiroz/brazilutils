/*
 * Created on 08/05/2005
 */
package org.brazilutils.test;

import junit.framework.TestCase;

import org.brazilutils.br.uf.UF;
import org.brazilutils.br.uf.ie.InscricaoEstadual;
import org.brazilutils.br.uf.ie.InscricaoEstadualAC;
import org.brazilutils.br.uf.ie.InscricaoEstadualRJ;
import org.brazilutils.br.uf.ie.InscricaoEstadualTO;

/**
 * @author Douglas Siviotti
 */
public class UFInscricaoEstadualTest extends TestCase {

    public void testUfName() throws Exception {
        UF uf = null;
		//uf = UF.AC; // cria direto ou ...
		uf = UF.valueOf("AC"); // Cria a UF a partir da Sigla
		assertTrue(uf.getUfName().equals("Acre"));
		assertTrue(uf.toString().equals("AC"));
    }

	public void testUfInscricaoEstadual() throws Exception {
        UF uf = null;
		//uf = UF.AC; // cria direto ou ...
		uf = UF.valueOf("AC"); // Cria a UF a partir da Sigla		 
        uf.getInscricaoEstadual().setNumber("01.004.823/001-12");
        assertTrue(uf.getInscricaoEstadual().isValid());
        // SP
        uf = UF.SP;
        InscricaoEstadual ie = uf.getInscricaoEstadual();
        ie.setNumber("110.042.490.114");
        assertTrue(ie.isValid());
        // MG
        ie = UF.MG.getInscricaoEstadual();
        ie.setNumber("062.307.904/0081");
        assertTrue(ie.isValid());
        
        // ------ IEs Passadas pelo Renato para teste na aplicacao
        ie = UF.AC.getInscricaoEstadual();  
        ie.setNumber("0100482300112");
        assertTrue(ie.isValid());
        
        ie = UF.AL.getInscricaoEstadual();  
        ie.setNumber("240534514");
        assertTrue(ie.isValid());
        
        ie = UF.AP.getInscricaoEstadual(); 
        ie.setNumber("030123459");
        assertTrue(ie.isValid());
        
        ie = UF.AM.getInscricaoEstadual(); 
        ie.setNumber("041171616");
        assertTrue(ie.isValid());
        
        ie = UF.BA.getInscricaoEstadual();   
        ie.setNumber("12345663");
        assertTrue(ie.isValid());
        
        ie = UF.CE.getInscricaoEstadual();   
        ie.setNumber("060000015");
        assertTrue(ie.isValid());
        
        ie = UF.DF.getInscricaoEstadual(); 
        ie.setNumber("0730000100109");
        assertTrue(ie.isValid());
        
        ie = UF.ES.getInscricaoEstadual();   
        ie.setNumber("080049400");
        assertTrue(ie.isValid());
        
        ie = UF.GO.getInscricaoEstadual();   
        ie.setNumber("109876547");
        assertTrue(ie.isValid());
        ie.setNumber("103218823");
        assertTrue(ie.isValid());
        
        ie = UF.MA.getInscricaoEstadual();   
        ie.setNumber("120000385");
        assertTrue(ie.isValid());
        ie.setNumber("121507343");
        assertTrue(ie.isValid());
        ie.setNumber("121741524");
        assertTrue(ie.isValid());
        
        ie = UF.MT.getInscricaoEstadual();  
        ie.setNumber("00130000019");
        assertTrue(ie.isValid());
        
        ie = UF.MS.getInscricaoEstadual();  
        ie.setNumber("283295570");
        assertTrue(ie.isValid());
        ie.setNumber("283264128");
        assertTrue(ie.isValid());
        ie.setNumber("282934073");
        assertTrue(ie.isValid());
        
        ie = UF.MG.getInscricaoEstadual();  
        ie.setNumber("0623079040081");
        assertTrue(ie.isValid());
        
        ie = UF.PA.getInscricaoEstadual();  
        ie.setNumber("159999995");
        assertTrue(ie.isValid());
        ie.setNumber("151801266");
        assertTrue(ie.isValid());
                
        ie = UF.PB.getInscricaoEstadual();  
        ie.setNumber("060000015");
        assertTrue(ie.isValid()); 
        ie.setNumber("160020956");
        assertTrue(ie.isValid());
        
        ie = UF.PR.getInscricaoEstadual();  
        ie.setNumber("1234567850");
        assertTrue(ie.isValid());
        
        ie = UF.PE.getInscricaoEstadual();
        ie.setNumber("18100100000049");
        assertTrue(ie.isValid());
        ie.setNumber("032141840");
        assertTrue(ie.isValid());
        
        ie = UF.PI.getInscricaoEstadual();  
        ie.setNumber("012345679");
        assertTrue(ie.isValid());
        
        ie = UF.RJ.getInscricaoEstadual();  
        ie.setNumber("84628328");
        assertTrue(ie.isValid());
        ie.setNumber("02927527");
        assertTrue(ie.isValid());
        
        ie = UF.RN.getInscricaoEstadual();  
        ie.setNumber("200400401");
        assertTrue(ie.isValid());
        ie.setNumber("2000400400");
        assertTrue(ie.isValid());
        
        ie = UF.RO.getInscricaoEstadual();  
        ie.setNumber("101625213");
        assertTrue(ie.isValid());
        ie.setNumber("00000000475203");
        assertTrue(ie.isValid());
        ie.setNumber("00000000625213");
        assertTrue(ie.isValid());
        
        ie = UF.RR.getInscricaoEstadual();  
        ie.setNumber("240066281");
        assertTrue(ie.isValid());
        ie.setNumber("24006153-6");
        assertTrue(ie.isValid());
        ie.setNumber("24007356-2");
        assertTrue(ie.isValid());
                
        ie = UF.RS.getInscricaoEstadual();    
        ie.setNumber("2243658792");
        assertTrue(ie.isValid());        
        
        ie = UF.SE.getInscricaoEstadual();  
        ie.setNumber("271234563");
        assertTrue(ie.isValid());
                
        ie = UF.SP.getInscricaoEstadual();  
        ie.setNumber("110042490114");
        assertTrue(ie.isValid());
        
        ie = UF.TO.getInscricaoEstadual();  
        ie.setNumber("29010227836");
        assertTrue(ie.isValid());
        
        ie = UF.SC.getInscricaoEstadual();    
        ie.setNumber("251040852");
        assertTrue(ie.isValid());
        
    }
    
	/**
	 * @author Rafael Fiume
	 */
	public void testChainValidator() {		
		final InscricaoEstadual ac = new InscricaoEstadualAC();
		final InscricaoEstadual rj = new InscricaoEstadualRJ(); 
		final InscricaoEstadual to = new InscricaoEstadualTO();
		
		// Ajusta a cadeia: RJ -> TO -> AC
		rj.addValidator(to);
		to.addValidator(ac);
		
		// Qual a IE que queremos validar?
		
		// Primeiro testemos uma IE de MG, que não faz parte da cadeia.
		assertFalse(rj.validate("062.307.904/0081"));
		
		// Agora sim, uma IE que faz parte da cadeia. Do último elo, aliás.
		assertTrue(rj.validate("01.004.823/001-12")); 
	}
}

/*
 * Created on 10/05/2005
 */
package org.brazilutils.test;

import junit.framework.TestCase;

import org.brazilutils.br.cpfcnpj.Cnpj;
import org.brazilutils.br.cpfcnpj.Cpf;
import org.brazilutils.br.cpfcnpj.CpfCnpj;

/**
 * @author Douglas Siviotti
 */
public class CpfCnpjTest extends TestCase {

	public void test() throws Exception {
		final CpfCnpj c = new CpfCnpj();

		c.setCpfCnpj("29520590000165");
		assertTrue(c.isValid()); // CNPJ valid
		assertTrue(c.isCnpj()); // is CNPJ
		assertFalse(c.isCpf()); // is not CPF
		// Valida a aplica��o correta da mascara de CPF
		assertEquals(CpfCnpj.CPF_MASK.length(), c.getCpfCnpj().length());

		c.setCpfCnpj("12345678911");
		assertFalse(c.isValid());// CPF invalid
		assertFalse(c.isCnpj()); // is not CNPJ
		assertTrue(c.isCpf()); // is CPF
		// Valida a aplica��o correta da mascara de CPF
		assertEquals(CpfCnpj.CNPJ_MASK.length(), c.getCpfCnpj().length());

		// System.out.println(c.toString());
		assertTrue(c.toString().equals("123.456.789-11"));

		c.setCpfCnpj("123456789"); // number invalid
		assertFalse(c.isValid());// format invalid
		assertFalse(c.isCnpj()); // is not CNPJ
		assertFalse(c.isCpf()); // is not CPF
	}

	// Adicionado por Rafael Fiume - 08/03/2007
	public void testCpf() throws Exception {
		final Cpf cpf = new Cpf("217.505.168-45");
		assertTrue(cpf.isValid());
		assertTrue(Cpf.isValid("217.505.168-45"));
	}

	// Adicionado por Rafael Fiume - 08/03/2007
	public void testCnpj() throws Exception {
		final Cnpj cnpj = new Cnpj("29.520.590/0001-65");
		assertTrue(cnpj.isValid());
		assertTrue(Cnpj.isValid("29520590000165"));
	}
}

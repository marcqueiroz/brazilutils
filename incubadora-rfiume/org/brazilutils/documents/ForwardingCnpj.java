package org.brazilutils.documents;

/**
 * TODO 1 Documentar classe.
 * 
 * TODO 2 Criar exemplo de utilização de ForwardingCnpj.
 * 
 * Ver Effective java, 2° Edition, Item 16.
 * 
 * 
 * @author Rafael Fiume
 * @version 1.0 - 29/03/2009
 */
public class ForwardingCnpj extends Cnpj {
    
    private final Cnpj cnpj;
    
    /*
     * Nota: utilizar Cnpj como parâmetro assegura que encapsulará um CNPJ válido.
     */
    public ForwardingCnpj(final Cnpj cnpj) {
        this.cnpj = cnpj; 
    }

    public String getId() {
        // TODO Auto-generated method stub
        return null;
    }

    public int compareTo(Object other) {
        // TODO Auto-generated method stub
        return 0;
    }
    
}

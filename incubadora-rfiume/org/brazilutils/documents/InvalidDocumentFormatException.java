package org.brazilutils.documents;

/**
 * Lançada quando o formato de um documento é invalido.
 * <p>
 * 
 * @author Rafael Fiume
 * @version 1.0 - 21/06/2008
 */
public class InvalidDocumentFormatException extends InvalidDocumentException {
    
    public InvalidDocumentFormatException() {
        super();
    }
    
    public InvalidDocumentFormatException(String message) {
        super(message);
    }
    
    public InvalidDocumentFormatException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public InvalidDocumentFormatException(Throwable cause) {
        super(cause);
    }
}

package org.brazilutils.documents;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;


/**
 * Representa um CPF. <br>
 * 
 * Objeto imutável, de acordo com as recomendações do Effective Java, 2° Ed; Item 15.
 * 
 * @author Douglas Siviotti, Rafael Fiume
 * @version 2.0 - 22/06/2008
 */
public abstract class Cpf implements Document, Comparable {
   
    /**
     * Retorna uma nova instância de <tt>Cpf</tt>.
     * 
     * @param id A identificação do CPF <b>não</b> formatada.
     * 
     * @throws InvalidDocumentFormatException Caso o formato da identificação do CPF seja inválido.
     */
    public static Cpf valueOf(final String id) {
        return new CpfImpl(id);
    }
    
    /**
     * Retorna uma nova instância de <tt>Cpf</tt>.
     * 
     * @param id A identificação do CPF formatada.
     * 
     * @throws InvalidDocumentFormatException Caso o formato da identificação do CPF seja inválido.
     */
    public static Cpf valueOfFormatted(final String id) {
        return new CpfImpl(Cpf.shorten(id));
    }
    
    /**
     * Utilitário para verficar se uma <tt>String<tt> representa um CPF sem formatação válido.
     */
    public static boolean isValid(String cpf) {
        return false; // TODO
    }
    
    /**
     * Utilitário para verficar se uma <tt>String<tt> representa um CPF formatado válido.
     */
    public static boolean isValidFormatted(String cpf) {
        return false; // TODO
    }
    
    /**
     * Formata CPF não-formatado. Ex:
     * <p>
     * <code>String cpf = CPF.format("11111111111");
     * <br>System.out.print(cpf);</code>
     * <p>
     * O output será <code>111.111.111-11</code>.
     * <p>
     * 
     * @throws InvalidDocumentFormatException
     *             se <tt>id</tt> não seguir o padrão de um CPF.
     */
    public static String format(String id) {
        return null; // TODO 
    }
    
    /**
     * Extrai a formatação de um CPF. Ex:
     * <p>
     * <code>String cpf = CPF.shorten("111.111.111-11");
     * <br>System.out.print(cpf);</code>
     * <p>
     * O output será <code>11111111111</code>.
     * 
     * @throws InvalidDocumentFormatException
     *             se <tt>id</tt> não seguir o padrão de um CPF.
     */
    public static String shorten(String id) {
        return null; // TODO 
    }
    
    Cpf() {
        // Torna Cpf final para classes fora do pacote org.brazilutils.documents.
    }
    
    public abstract String getId();
    
    
    /**
     * Implementação padrão de <tt>Cpf</tt>.
     * <p>
     * <b>Nota:</b> Métodos <tt>hashCode</tt> e <tt>toString</tt> utilizam <i>lazy caching</i>.
     * Isto é, armazenam em cache o resultado da chamada desses métodos após a primeira chamada.
     * Posteriores requisições desses método retornarão os resultados em cache. Ver Effective Java,
     * 2° Ed.; Itens 15, 71.
     */
    static class CpfImpl extends Cpf {
        
        /**
         * A id do documento, definida de acordo com as regras do governo.
         */
        private final String id;
        
        private String formattedCpf;
        
        private int hashCode;
        
        /**
         * Contrutor <tt>package-private</tt> torna a classe final para o usuário da classe, mas ainda
         * possibilita aos desenvolvedores estenderem a classe caso necessário, como, por exemplo, para
         * aumentar a performance.
         * 
         * @param id A identificação do CPF <b>não</b> formatada.
         * 
         * @throws InvalidDocumentFormatException Caso o formato da identificação do CPF seja inválido.
         */
        CpfImpl(final String id) {
            if (!Cpf.isValid(id)) {
                throw new InvalidDocumentFormatException(
                        new StringBuffer("Id ").append(id).append(" inválida para criação de CPF.").toString());
            }
            
            this.id = id;
        }
        
        public String getId() {
            return this.id;
        }
        
        public int compareTo(Object arg0) {
            // TODO 
            // 1. Deve permitir a ordenação de CPF formatados ou não.
            // 2. Se dois CPFs forem idênticos, o CPF formatados ou não.
            return 0;
        }
        
        /**
         * Retorna a representação formatada de um CPF. O resultado deverá ser algo como
         * <code>318.125.168-33</code>.
         */
        public String toString() {
            if (StringUtils.isEmpty(this.formattedCpf)) {
                formattedCpf = Cpf.format(this.id);
            }
            
            return formattedCpf;
        }
        
        public boolean equals(final Object other) {
            if (other == null) { 
                return false; 
            }
            if (other == this) { 
                return true; 
            }
            if (other.getClass() != this.getClass()) {
              return false;
            }

            final CpfImpl otherCpf = (CpfImpl)other;
            
            return new EqualsBuilder().append(this.id, otherCpf.id).isEquals();
        }
        
        public int hashCode() {
            if (hashCode == 0) {
                hashCode = new HashCodeBuilder().append(this.id).toHashCode();
            }
            
            return hashCode;
        }
        
    }
           
}

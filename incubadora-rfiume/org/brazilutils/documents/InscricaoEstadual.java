package org.brazilutils.documents;


/**
 * Inscrição estadual de um dos estados da nação.
 * 
 * PENDING Static method factory como conveniência para obter uma IE?
 * 
 * @author Rafael Fiume
 * @version 1.0 - 20/06/2008
 */
public class InscricaoEstadual implements Document {
    
    private final String id;
    
    // PENDING Alterar tipo para typesafe enum para estado
    private final String estado;
    
    private InscricaoEstadual(String id, String estado) {
        this.id = id;
        this.estado = estado;
    }
    
    public String getId() {
        return null;
    }
    
    
}

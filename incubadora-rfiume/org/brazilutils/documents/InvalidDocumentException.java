package org.brazilutils.documents;

/**
 * Indica a tentativa de criação de um documento inválido.
 * <p>
 * PENDING Estende RuntimeException?
 * 
 * @author Rafael Fiume
 * @version 1.0 - 28/03/2009
 */
public class InvalidDocumentException extends RuntimeException {
    
    public InvalidDocumentException() {
        super();
    }
    
    public InvalidDocumentException(String message) {
        super(message);
    }
    
    public InvalidDocumentException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public InvalidDocumentException(Throwable cause) {
        super(cause);
    }
    
}

package org.brazilutils.documents;



/**
 * Documentos (e.g. Cpf, Rg, Cnh) implementam essa interface. <br>
 * <p>
 * <h2>Contrato de um <tt>Document</tt></h2>
 * Documentos devem ser objetos imutáveis. <br>
 * PENDING Explicar o porquê disso.
 * <p>
 * Note que, como um objeto <tt>Document</tt> é imutável, subsequentes chamadas à <tt>isValid</tt>
 * sempre retornará o mesmo resultado.
 * 
 * @see Cpf
 * 
 * @author Rafael Fiume
 * @version 1.0 - 20/06/2008
 */
public interface Document {
    
    /**
     * O ID de identificação do documento.
     * 
     * @return
     */
    String getId();
    
}

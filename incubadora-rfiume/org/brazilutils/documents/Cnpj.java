package org.brazilutils.documents;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Representa um CNPJ. <br>
 * 
 * Objeto imutável, de acordo com as recomendações do Effective Java, 2° Ed; Item 15.
 * 
 * @author Douglas Siviotti, Rafael Fiume
 * @version 2.0 - 29/03/2009
 */
public abstract class Cnpj implements Document, Comparable {
    
    /**
     * Retorna uma nova instância de <tt>Cnpj</tt>.
     * 
     * @param id A identificação do CNPJ <b>não</b> formatada.
     * 
     * @throws InvalidDocumentFormatException Caso o formato da identificação do CNPJ seja inválido.
     */
    public static Cnpj valueOf(final String id) {
        return new CnpjImpl(id);
    }
    
    /**
     * Retorna uma nova instância de <tt>Cnpj</tt>.
     * 
     * @param id A identificação do CNPJ formatada.
     * 
     * @throws InvalidDocumentFormatException Caso o formato da identificação do CNPJ seja inválido.
     */
    public static Cnpj valueOfFormatted(final String id) {
        return new CnpjImpl(Cnpj.shorten(id));
    }

    /**
     * Utilitário para verficar se uma <tt>String<tt> representa um CNPJ sem formatação válido.
     */
    public static boolean isValid(String cnpj) {
        return false; // TODO
    }
    
    /**
     * Utilitário para verficar se uma <tt>String<tt> representa um CNPJ formatado válido.
     */
    public static boolean isValidFormatted(String cnpj) {
        return false; // TODO
    }
    
    Cnpj() {
        // Torna Cnpj final para classes fora do pacote org.brazilutils.documents.
    }
    
    public abstract String getId();
    
    /**
     * Formata CNPJ não-formatado. 
     * 
     * @throws InvalidDocumentFormatException
     *             se <tt>id</tt> não seguir o padrão de um CNPJ.
     */
    public static String format(String id) {
        return null; // TODO 
    }
    
    /**
     * Extrai a formatação de um CNPJ.
     * 
     * @throws InvalidDocumentFormatException
     *             se <tt>id</tt> não seguir o padrão de um CNPJ.
     */
    public static String shorten(String id) {
        return null; // TODO 
    }
    
    
    /**
     * Implementação padrão de <tt>Cnpj</tt>. <p>
     * 
     * <b>Nota:</b> Métodos <tt>hashCode</tt> e <tt>toString</tt> utilizam <i>lazy caching</i>.
     * Isto é, armazenam em cache o resultado da chamada desses métodos após a primeira chamada.
     * Posteriores requisições desses método retornarão os resultados em cache. Ver Effective Java,
     * 2° Ed.; Itens 15, 71.
     */
    static class CnpjImpl extends Cnpj {
        
        /**
         * A id do documento, definida de acordo com as regras do governo. 
         */
        private final String id;
        
        private String formattedCnpj;
        
        private int hashCode;
        
        /**
         * Contrutor <tt>package-private</tt> torna a classe final para o usuário da classe, mas ainda
         * possibilita aos desenvolvedores estenderem a classe caso necessário, como, por exemplo, para
         * aumentar a performance.
         * 
         * @param id A identificação do CNPJ <b>não</b> formatada.
         * 
         * @throws InvalidDocumentFormatException Caso o formato da identificação do CNPJ seja inválido.
         */
        CnpjImpl(final String id) {
            if (!Cnpj.isValid(id)) {
                throw new InvalidDocumentFormatException(
                        new StringBuffer("Id ").append(id).append(" inválida para criação de CNPJ.").toString());
            }
            
            this.id = id;
        }
        
        public String getId() {
            return id;
        }

        public int compareTo(Object arg0) {
            // TODO 
            // 1. Deve permitir a ordenação de CNPJs formatados ou não.
            // 2. Deve retornar 0 caso os CNPJs sejam idênticos, formatados ou não.
            return 0;
        }
        
        /**
         * Retorna a representação formatada de um CNPJ. O resultado deverá ser algo como
         * <code>??? ??? ??? ???</code>. 
         * 
         * TODO Documentar melhor o método. 
         */
        public String toString() {
            if (StringUtils.isEmpty(this.formattedCnpj)) {
                formattedCnpj = Cnpj.format(this.id);
            }
            
            return formattedCnpj;
        }
        
        public boolean equals(final Object other) {
            if (other == null) { 
                return false; 
            }
            if (other == this) { 
                return true; 
            }
            if (other.getClass() != this.getClass()) {
              return false;
            }

            final CnpjImpl otherCnpj = (CnpjImpl)other;
            
            return new EqualsBuilder().append(this.id, otherCnpj.id).isEquals();
        }
        
        public int hashCode() {
            if (hashCode == 0) {
                hashCode = new HashCodeBuilder().append(this.id).toHashCode();
            }
            
            return hashCode;
        }
        
    }
        
}

package org.brazilutils.address;


/**
 * Representa telefone.
 * 
 * @author Rafael Fiume
 * @version 1.0 - 31/01/2009
 */
public interface Phone {
    
    // TODO providenciar interfaces para que o usuário implemente o domínio da maneira que quiser?
    
    // TODO criar implementação final por meio de contrutores private/package-private
    // TODO criar factory - ver Effective Java, Item 15, pág. 77/78
    
    /**
     * @return O número do telefone.
     */
    String getNumber();
    
    /**
     * @return O código regional do número do telefone.
     */
    String getRegionalCode();
    
    /**
     * @return O código internacional do número do telefone.
     */
    String getInternationalCode();
    
    String format();
}

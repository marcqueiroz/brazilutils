package org.brazilutils.address;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * Lista de telefones, contém operações para manipular conjunto de telefones.
 * <p>
 * Utilizada nos casos em que uma pessoa, cliente, empresa, ou o que for, tenham mais de um telefone
 * para contato.
 * <p>
 * <b>Nota sobre o nome da entidade:</b> Apesar do nome convencional "lista", <tt>PhoneList</tt> é na verdade
 * um conjunto, como um catálogo de telefone deve ser.
 * <p>
 * 
 * TODO List: - Deve especificar e validar o limite de número de telefones na lista.
 * 
 * @author Rafael Fiume
 * @version 1.0 - 31/01/2009
 */
public class PhoneList implements Set {
    
    // TODO tornar a classe final por meio de contrutores private/package-private
    // TODO criar factory - ver Effective Java, Item 15, pág. 77/78

    /**
     * O limite do número de telefones.
     */
    private int limite;
    
    public boolean add(Object arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean addAll(Collection arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    public void clear() {
        // TODO Auto-generated method stub
        
    }

    public boolean contains(Object o) {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean containsAll(Collection arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean isEmpty() {
        // TODO Auto-generated method stub
        return false;
    }

    public Iterator iterator() {
        // TODO Auto-generated method stub
        return null;
    }

    public boolean remove(Object o) {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean removeAll(Collection arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean retainAll(Collection arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    public int size() {
        // TODO Auto-generated method stub
        return 0;
    }

    public Object[] toArray() {
        // TODO Auto-generated method stub
        return null;
    }

    public Object[] toArray(Object[] arg0) {
        // TODO Auto-generated method stub
        return null;
    }
    
}

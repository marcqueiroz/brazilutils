package org.brazilutils.money;

import java.text.ParseException;

/**
 * Representa valor financeiro.
 * 
 * @author Rafael Fiume
 * @version 1.0 - 31/01/2009
 */
public interface Money {
    
    // TODO providenciar interfaces para que o usuário implemente o domínio da maneira que quiser?
    
    // TODO criar implementação final por meio de contrutores private/package-private
    // TODO criar factory - ver Effective Java, Item 15, pág. 77/78
    
    Money getValorInteiro(); // Passar nome do método para inglês.
    
    Money getCents();
    
    void plus(Money money);
    
    void subtract(Money money);
    
    void multiply(Money money);
    
    void divide(Money money);
    
    /**
     * @return O Valor monetário por extenso.
     */
    String toText();
    
    /**
     * @return O Valor monetário formatado.
     */
    String format();
    
    /**
     * 
     * @param money Representação textual do valor monetário.
     * 
     * @return O valor monetário <tt>Money</tt>.
     * 
     * @throws ParseException Quando não for possível fazer o parse.
     */
    Money parse(String money) throws ParseException;
}
